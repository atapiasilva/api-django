"""agilesoftapi URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import include, url

from rest_framework import routers

from todo.serializers import TaskViewSet, UserViewSet, TaskUpdateViewSet
from todo.views import TaskListAPIView, UserAPICreateView, TaskAPICreateView, TaskAPIUpdateView

router = routers.DefaultRouter()
router.register(r"tasks", TaskViewSet, basename='Task')
router.register(r"users", UserViewSet, basename='User')
router.register(r"tasks-update", TaskUpdateViewSet, basename='TaskUpdate')

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/auth', include('rest_framework.urls', namespace='rest_framework')),
    path('api/', include(router.urls)),
    path('api/tasks/', TaskListAPIView.as_view(), name= 'task_list_api'),
    path('api/users/create', UserAPICreateView.as_view(), name='user_create_api'),
    path('api/tasks/create', TaskAPICreateView.as_view(), name='task_create_api'),
    path('api/tasks/update/<int:pk>', TaskAPIUpdateView.as_view(), name='task_update_api')

]
