from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework import routers, serializers, viewsets, permissions
from django.contrib.auth.models import User

from .models import Task

class UserCreateSerializer(serializers.ModelSerializer):
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [permissions.IsAuthenticated,]
    class Meta:
        model = User
        fields = [
            'username',
            'password',
            'first_name'
        ]

class UserSerializer(serializers.HyperlinkedModelSerializer):
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [permissions.IsAuthenticated,]
    class Meta:
        model = User
        fields = [
            'id',
            'username',
            'password',
            'first_name'
        ]

class UserViewSet(viewsets.ModelViewSet):
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [permissions.IsAuthenticated,]
    queryset = User.objects.all()
    serializer_class = UserSerializer


class TaskUpdateSerializer(serializers.ModelSerializer):
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [permissions.IsAuthenticated,]
    user = serializers.PrimaryKeyRelatedField(queryset=User.objects.all())
    class Meta:
        model = Task
        fields = [
            'user',
            'status',
            'description'
        ]

class TaskCreateSerializer(serializers.ModelSerializer):
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [permissions.IsAuthenticated,]
    user = serializers.PrimaryKeyRelatedField(queryset=User.objects.all())
    class Meta:
        model = Task
        fields = [
            'user',
            'status',
            'description'
        ]

class TaskSerializer(serializers.HyperlinkedModelSerializer):
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [permissions.IsAuthenticated,]
    user = serializers.PrimaryKeyRelatedField(queryset=User.objects.all())
    class Meta:
        model = Task
        fields = [
            'id',
            'user',
            'status',
            'description'
        ]


class TaskUpdateViewSet(viewsets.ModelViewSet):
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [permissions.IsAuthenticated,]
    queryset = Task.objects.all()
    serializer_class = TaskUpdateSerializer

class TaskViewSet(viewsets.ModelViewSet):
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [permissions.IsAuthenticated,]
    queryset = Task.objects.all()
    serializer_class = TaskSerializer