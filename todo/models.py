from django.db import models
from django.contrib.auth.models import User
# Create your models here.

class Task(models.Model):

    TASK_STATUS = (
        ('1','Resuelto'),
        ('2', 'No Resuelto')
    )

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    status = models.CharField(max_length=1, choices=TASK_STATUS)
    description = models.TextField(max_length=100)


    