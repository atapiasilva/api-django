from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework import routers, serializers, viewsets, permissions, generics
from django.shortcuts import render
from rest_framework import generics

from .models import Task
from .serializers import TaskSerializer, UserCreateSerializer, TaskCreateSerializer, TaskUpdateSerializer


class UserAPICreateView(generics.CreateAPIView):
    serializers_class = UserCreateSerializer


class TaskAPIUpdateView(generics.UpdateAPIView):
    queryset = Task.objects.all()
    serializers_class = TaskUpdateSerializer
    lookup_field = 'id'

class TaskAPICreateView(generics.CreateAPIView):
    serializers_class = TaskCreateSerializer

class TaskListAPIView(generics.ListAPIView):
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [permissions.IsAuthenticated]
    queryset = Task.objects.all()
    serializer_class =  TaskSerializer
    paginate_by = 10